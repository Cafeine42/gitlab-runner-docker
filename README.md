# Gitlab-runner docker

## Quick start

### Requirement :
- docker
- docker-compose

## Launch :

Launch docker container :
```docker-compose up -d```

Move inside container :
```docker exec -it gitlab-runner_gitlab-runner_1 bash```

Register new runner (change HOME_PATH and TOKEN before)
HOME_PATH must be your host path !
```
gitlab-runner register \
      --non-interactive \
      --name "Local Runner" \
      --url "https://gitlab.com/" \
      --registration-token "TOKEN" \
      --executor "docker" \
      --docker-pull-policy "if-not-present"\
      --docker-image alpine:latest \
      --docker-volumes 'HOME_PATH/gitlab-runner/cache:/cache:rw' \
      --docker-volumes '/var/run/docker.sock:/var/run/docker.sock'\
      --tag-list "docker" \
      --run-untagged="true" \
      --locked="false"
```
